#!/usr/bin/python3

from socket import SOCK_STREAM, AF_INET6
import random, socket, select, threading, sys

def connexion():

	# Saisie de l'adresse IP et du port du serveur
	HOST = input("Adresse IP : ")
	port = input("Numéro de port : ")

	PORT = int(port) # convertion du port saisie en nombre entier

	# Protocole TCP
	serveurS = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) # création socket
	serveurS.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # option socket
	print("Création du socket")

	try:
		serveurS.bind((HOST, PORT)) # affectation du socket au serveur
		print("Serveur prêt sur le port", PORT, ", en attente de client...")
		serveurS.listen(1) # serveur en écoute
		while True:
			clientS, adresse = serveurS.accept() # accepter connexion
	except socket.error:
		print("La liaison du socket a échoué !")
		sys.exit()


connexion()
