#!/usr/bin/python3

from grid import *
from socket import SOCK_STREAM, AF_INET6
import random, socket, select, threading, sys

def connexionserveur():

	# Saisie de l'adresse IP et du port du serveur afin de s'y connecter
	HOST = input("Adresse IP du serveur : ")
	port = input("Numéro de port : ")

	PORT = int(port) # convertion du port saisie en nombre entier

	# Protocole TCP
	clientS = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) # création socket
	clientS.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # option socket
	print("Création du socket")

	try:
		clientS.connect((HOST, PORT)) # connexion au serveur
		print("Connexion établie avec le serveur sur le port", PORT)
	except socket.error:
		print("La connexion avec le serveur a échoué !")
		sys.exit()

	print("Lancement de la partie...")

def main():
	grids = [grid(), grid(), grid()]
	current_player = J1
	grids[J1].display()
	while grids[0].gameOver() == -1:
		if current_player == J1:
			shot = -1
			while shot <0 or shot >=NB_CELLS:
				shot = int(input ("quel case allez-vous jouer ?"))
		else:
			shot = random.randint(0,8)
			while grids[current_player].cells[shot] != EMPTY:
				shot = random.randint(0,8)
		if (grids[0].cells[shot] != EMPTY):
			grids[current_player].cells[shot] = grids[0].cells[shot]
		else:
			grids[current_player].cells[shot] = current_player
			grids[0].play(current_player, shot)
			current_player = current_player%2+1
		if current_player == J1:
			grids[J1].display()
	print("game over")
	grids[0].display()
	if grids[0].gameOver() == J1:
		print("you win !")
	else:
		print("you loose !")

connexionserveur()
main()
